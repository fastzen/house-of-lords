# Given a page URL that contains a person's profile, extract their personal information.
#
# Example use:
# member_object = member("http://www.parliament.uk/biographies/lords/baroness-adams-of-craigielea/631")
# puts member_object
def member(url)

    person = Hash.new

    person[:url] = url

    page = Nokogiri::HTML(open(url))

    # Name without 'The'
    titled_name_element = page.css("div#content-small h1")
    person[:titled_name] = titled_name_element[0].text

    # Get the gender title
    if person[:titled_name].start_with?("Baroness", "Lady")
        person[:gender_title] = "Mrs"
    elsif person[:titled_name].start_with?("Lord", "Viscount", "Earl", "Bishop", "Duke", "Marquess")
        person[:gender_title] = "Mr"
    else
        person[:gender_title] = "No gender title"
    end

    # Biog
    member_details = page.css("div#basic-details div.member-details p")
    person[:long_name] = member_details[0].text
    person[:real_name] = member_details[1].text
    person[:party] = member_details[2].text
    person[:start_date] = member_details[3].text

    # Create a CouchDB ID for this person, based upon their titled name
    id = person[:titled_name].downcase.gsub ' ', '-'

    person["_id"] = id

    return person
end

# Save or create a CouchDB document
def save_or_create(db, doc)
    begin
      rev = db.get(doc['_id'])['_rev']
      doc['_rev'] = rev
      db.save_doc(doc)
    rescue RestClient::ResourceNotFound => nfe
      db.save_doc(doc)
    end
end

# Loop through every row (ignoring header rows), go to the URL, extract data and store it into CouchDB
# There's a bit of sleeping included so that we don't hammer the website.
def process_rows(db, rows)

    rows.each do |row, index|
        hrefs = row.css("td a").map{ |a|
            a['href'] if !a['href'].match("#top")
        }.compact.uniq

        hrefs.each do |href|
            sleep 2
            member_object = member(href)
            puts member_object["_id"]
            save_or_create(db, member_object)
        end
        sleep 1 + rand(6)
    end

end