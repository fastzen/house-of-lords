require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'couchrest'
require './couchdb-url.rb'
require './lords-common.rb'

MEMBERS_URL = "http://www.parliament.uk/mps-lords-and-offices/lords/"
INELIGIBLE_MEMBERS_URL = "http://www.parliament.uk/mps-lords-and-offices/lords/-ineligible-lords/"

# COUCHDB_URL must be defined in ./couchdb-url.rb in the form:
# COUCHDB_URL = "https://username:password@server:port/database"
@db = CouchRest.database(COUCHDB_URL)

# Main routine starts here
page = Nokogiri::HTML(open(MEMBERS_URL))
rows = page.css('div#pnlListing table tbody tr')
process_rows(@db, rows)

# Main routine starts here
page = Nokogiri::HTML(open(INELIGIBLE_MEMBERS_URL))
rows = page.css('table#ineligibleLords tbody tr')
process_rows(@db, rows)