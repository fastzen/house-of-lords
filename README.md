# Lords

Some scripts for extracting information about UK House of Lords members and storing it into a CouchDB instance

## Requirements

* Ruby
* Gems
* nokogiri
* open-uri
* couchrest
* A CouchDB instance

## Pre-flight

COUCHDB_URL must be defined in ./couchdb-url.rb in the form:
COUCHDB_URL = "https://username:password@server:port/database"

## Take-off

ruby lords.rb